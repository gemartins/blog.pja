<header class="header">
  <div class="container">

    <a href="{{route('home')}}" class="logo"></a>

    <input class="menu-btn" type="checkbox" id="menu-btn" />
    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
    <ul class="menu">
      <li><a href="{{route('financa')}}">Finanças</a></li>
      <li><a href="{{route('venderereceber')}}">Vender e Receber</a></li>
      <li><a href="{{route('namidia')}}">Na Mídia</a></li>
      <li><a href="#" class="proteste b24-web-form-popup-btn-18">Proteste Já</a></li>
      <li><button type="button" name="button" class="btn-menu b24-web-form-popup-btn-18">Começar agora</button></li>
    </ul>
  </div>
</header>

<script id="bx24_form_button" data-skip-moving="true">
        (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                (w[b].forms=w[b].forms||[]).push(arguments[0])};
                if(w[b]['forms']) return;
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://pjasolucoes.bitrix24.com.br/bitrix/js/crm/form_loader.js','b24form');

        b24form({"id":"18","lang":"br","sec":"pwj6e0","type":"button","click":""});
      </script>
