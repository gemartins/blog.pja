<script src="{{ asset('theme/js/lib/fontawesome.js') }}" charset="utf-8"></script>
<script src="{{ asset('theme/js/lib/all.js') }}" charset="utf-8"></script>

<footer>
  <div class="container rodape">
    <div class="row">

      <div class="col-md-3 col1">

        <img src="{{ asset('theme/images/logo.png')}}" alt="Logo PJA">

        <p><b>Nossa missão, com muita tecnologia e um time de pessoas incríveis, é ajudar as empresas a receberem o dinheiro que e seu!</b></p>
        <br>
        @if(isset($empresa->endereco))
          <span>{{$empresa->endereco}}</span>
        @endif
      </div>

      <div class="col-md-3 col2">
        <h4>Categorias</h4>

        <div class="links">
          <span><a href="{{ route('financa') }}">Finanças</a><br></span>
          <span><a href="{{ route('venderereceber') }}">Vender e Receber</a><br></span>
          <span><a href="{{ route('namidia') }}">Na Mídia</a><br></span>
        </div>

      </div>

      <div class="col-md-3 col3">
        <h4>Fale com a gente</h4>

        <div class="sac">
          @if(isset($empresa->telefone_credor))
            <p class="ca"><b>Atendimento ao Credor</b></p>
            <span class="txt-ca"><a href="tel:+55{{$empresa->telefone_credor}}">+55 {{$empresa->telefone_credor}}</a></span>
          @endif
          @if(isset($empresa->telefone_comercial))
            <p class="cn"><b>Atendimento Comercial</b></p>
            <span class="txt-cn"><a href="tel:+55{{$empresa->telefone_comercial}}">+55 {{$empresa->telefone_comercial}}</a></span>
          @endif
        </div>

      </div>

      <div class="col-md-3 col4">
        <h4>Siga nossas redes sociais</h4>
        <div class="redesocial">
          <div class="container">
            @if(isset($empresa->facebook))
              <a href="{{$empresa->facebook}}"><img class="redesocial-img" src="{{asset('theme/images/facebook.svg')}}" alt="Facebook"></a>
            @endif
            @if(isset($empresa->instagram))
              <a href="{{$empresa->instagram}}"><img class="redesocial-img" src="{{asset('theme/images/instagram.svg')}}" alt="Instagram"></a>
            @endif
            @if(isset($empresa->linkedin))
              <a href="{{$empresa->linkedin}}"><img class="redesocial-img" src="{{asset('theme/images/linkedin.svg')}}" alt="Linkedin"></a>
            @endif
            @if(isset($empresa->youtube))
              <a href="{{$empresa->youtube}}"><img class="redesocial-img" src="{{asset('theme/images/youtube.svg')}}" alt="YouTube"></a>
            @endif
          </div>
        </div>

        <h4>Faça parte do nosso time</h4>
        <a href=""><div class="pjachados">#PJACHADOS</div></a>
      </div>

    </div>
  </div>

  <div class="container copyright" align="center">
    2020 Copyright © PJA.<br>Todos os Direitos Reservados.
  </div>
</footer>
