@extends('pages.base')

@section('metatags')
<meta property="og:title" content="#vendeReceber - Um movimento PJA."/>
<meta name="description" content="O BLog da Pja para tirar todas as suas dúvidas">
<meta property="og:image" content="{{ asset('theme/images/favicon.png') }}">
@endsection

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('theme/css/categorias.css') }}">
@endsection

@section('jslibrary')
  <script src="{{ asset('theme/js/lib/slick.js') }}" charset="utf-8"></script>
@endsection

@section('body')

    <!-- <div class="banner" style="background-image:linear-gradient(to top, rgba(26,214,112), rgba(34,168,89), rgba(34,124,67), rgba(30,82,46), rgba(21,44,27))"> -->
    <div class="banner" style="background-image:linear-gradient(167.38deg, rgba(26,214,112,0.8) 0%, rgba(33,105,52,0.8) 100%)">
    <div class="cabecalho">
      <div class="container">
        <span class="cabecalho-categoria">
          @if($post->categoria === "financas")
           Finanças
         @elseif($post->categoria === "vendereceber")
           Vender e Receber
         @else
           Na Mídia
         @endif
        </span>
      </div>
    </div>
  </div>

  <section class="posts">
    <div class="container">
      <div class="row">
        @foreach($posts as $post)<div class="col-md-4 post">
          <div class="card">
            <a href="{{ route('post.item', $post->slug)}}">
              <div class="post-image" style="background-image: url('http://pjasolucoes.com.br/uploads/post/{{$post->imagem}}');">
                <span class="categoria">
                  @if($post->categoria === "financas")
                    <a href="{{ route('financa')}}" class="cat-selecionada">Finanças</a>
                  @elseif($post->categoria === "vendereceber")
                    <a href="{{ route('venderereceber')}}" class="cat-selecionada">Vender e Receber</a>
                  @else
                    <a href="{{ route('namidia')}}" class="cat-selecionada">Na Mídia</a>
                  @endif
                </span>
              </div>
            </a>
            <div class="post-txt">
              @foreach($user->where('nome',$post->autor) as $autor)
                <div class="avatar" style="background-image: url('https://www.pjasolucoes.com.br/uploads/users/{{$autor->imagem}}')">
                </div>
              @endforeach
              <span class="autor-post">{{$post->autor}}</span>
              <a href="{{ route('post.item', $post->slug)}}">
                <h3>{{$post->titulo}}</h3>
              </a>
              <a href="{{ route('post.item', $post->slug)}}">
                <p class="post-resumo">{{ substr(strip_tags($post->texto), 0, 100) }}{{ (strlen(strip_tags($post->texto)) > 100 ? "..." : "") }}...</p>
              </a>
              <div class="cat-dat">
                <?php
                  date_default_timezone_set('America/Sao_Paulo');
                  setlocale(LC_ALL, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
                  setlocale(LC_TIME, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
                 ?>

                <span class="post-data">{{ Carbon\Carbon::parse($post->datapublicacao)->formatLocalized('%d de %B de %Y') }}</span>              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      {{ $posts->links() }}
    </div>
  </section>

  @include('pages.partials._footer')

@endsection

@section('script')
<script src="{{ asset('theme/js/jquery-1.9.1.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('theme/js/bootstrap.min.js') }}" charset="utf-8"></script>

@include('pages.partials._scripts')

@endsection
