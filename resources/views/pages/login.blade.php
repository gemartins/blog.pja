@extends('pages.base')

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('theme/css/login.css') }}">
  <link href="https://painel.pjasolucoes.com.br/static/temp/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
@endsection

@section('body')

<section class="login">
  <div class="container">
    <div class="row">
      <div class="formulario">

        <form class="" action="index.html" method="post">

          <div class="logo">
            <img src="https://pub.pjasolucoes.com.br/images/LOGO-FINAL3.png" alt="">
            <div class="destaque">
              <p>LOGIN</p>
            </div>
          </div>

          <input type="text" id="email" name="email" placeholder="Email"><br>
          <input type="password" id="senha" name="senha" placeholder="Senha"><br>

          <div class="lembrar">
            <input type="checkbox" name="remember" class="check">
            <label for="lembrar-check">Lembrar</label>
          </div>

          <div class="form-button">
            <button class="login" type="button" name="button">Entrar</button>
          </div>

          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong> Usuário ou senha inválidos </strong>
          </div>

          <!-- <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong></strong> Usuário ou senha inválidos
          </div> -->

        </form>

      </div>
    </div>
  </div>
</section>


@endsection

@section('script')
<script src="{{ asset('theme/js/bootstrap.min.js') }}" charset="utf-8"></script>
<script src="https://painel.pjasolucoes.com.br/static/temp/bootstrap/dist/js/bootstrap.min.js"></script>

@endsection
