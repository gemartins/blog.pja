<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('metatags')
    <meta property="og:url" content="https://pjajuda.pjasolucoes.com.br/">
    <meta name="author" content="Like Publicidade">
    <link rel="shortcut icon" href="{{ asset('theme/images/favicon.png') }}">

    <!-- Title Page-->
    <title>#vendeReceber</title>
    <meta name="keywords" content="PJA, cobrança, protesto, dividas, imperatriz"/>

    <!-- Fonts e GRID -->
    <link rel="stylesheet" href="{{ asset('theme/css/lib/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/fonts/fonts.css') }}">

    <!-- Styles Sheets -->
    <link rel="stylesheet" href="{{ asset('theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/footer.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/menu.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/banner.css') }}">

    @yield('stylesheet')

    <!-- JS Library's -->
    <script src="{{ asset('theme/js/lib/jquery.min.js') }}" charset="utf-8"></script>
    @yield('jslibrary')
</head>
<body>
    @include('pages.partials._menu')

    @yield('body')

    @yield('script')

</body>
</html>
