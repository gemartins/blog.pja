@extends('pages.base')

@section('metatags')
<title>{{$post->titulo}}</title>
<meta property="og:title" content="#vendeReceber"/>
<meta name="description" content="{{$post->titulo}}">
<meta property="og:image" content="asset('../../../uploads/post/{{$post->imagem}}')">
@endsection

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('theme/css/post.css') }}">
@endsection

@section('jslibrary')
  <script src="{{ asset('theme/js/lib/slick.js') }}" charset="utf-8"></script>
@endsection

@section('body')
    <div class="banner" style="background-image:linear-gradient(to top, rgba(26,214,112,0.8),rgb(26,214,112,0.8)), url('http://pjasolucoes.com.br/uploads/post/{{$post->imagem}}')">
    <div class="cabecalho">
      <div class="container">
          <div class="row">
            <div class="col-12">
            <span class="cabecalho-categoria">
              @if($post->categoria === "financas")
               Finanças
             @elseif($post->categoria === "vendereceber")
               Vender e Receber
             @else
               Na Mídia
             @endif
            </span><br>
              <span class="cabecalho-titulo">{{$post->titulo}}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section class="postagem">
    <div class="container">

      <div class="head">
        <div class="breadcrumb">
          <p class="rota"><a href="{{route('home')}}">Home</a> >
            @if($post->categoria === "financas")
              <a href="{{ route('financa')}}">Finanças</a>
            @elseif($post->categoria === "vendereceber")
              <a href="{{ route('venderereceber')}}">Vender e Receber</a>
            @else
              <a href="{{ route('namidia')}}">Na Mídia</a>
            @endif
          </p>
          <?php
            date_default_timezone_set('America/Sao_Paulo');
            setlocale(LC_ALL, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
            setlocale(LC_TIME, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
           ?>
          <p class="data">Publicado por {{$post->autor}} em {{Carbon\Carbon::parse($post->datapublicacao)->formatLocalized('%d de %B de %Y')}}</p>
        </div>
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
          <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
          <a class="a2a_button_facebook"></a>
          <a class="a2a_button_twitter"></a>
          <a class="a2a_button_linkedin"></a>
          <a class="a2a_button_whatsapp"></a>
        </div>
        <script async src="https://static.addtoany.com/menu/page.js"></script>
      </div>

      <div class="texto">
        {!! $post->texto !!}
      </div>
    </div>
  </section>

  @include('pages.partials._footer')

@endsection

@section('script')
<script src="{{ asset('theme/js/jquery-1.9.1.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('theme/js/bootstrap.min.js') }}" charset="utf-8"></script>

@include('pages.partials._scripts')

@endsection
