@extends('pages.base')

@section('metatags')
<meta property="og:title" content="#vendeReceber"/>
<meta name="description" content="Um movimento PJA para">
<meta property="og:image" content="{{ asset('theme/images/favicon.png') }}">
@endsection

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('theme/css/home.css') }}">
@endsection

@section('jslibrary')
  <script src="{{ asset('theme/js/lib/slick.js') }}" charset="utf-8"></script>
@endsection

@section('body')
  <!-- @include('pages.partials._loader') -->

  <section class="banner">
    @if(count($banners) > 0)
        <div class="carousel-banner" id="banner">
          @foreach($banners as $banner)
            <div class="item-carousel" style="background-image:linear-gradient(to top, rgba(26,214,112,0.8),rgb(26,214,112,0.8)), url('http://pjasolucoes.com.br/uploads/post/{{$banner->imagem}}')";{{$loop->first?'opacity:1':''}}">
              <div class="container">
                <div class="box">
                  <strong><h2>{{ substr(strip_tags($banner->titulo), 0, 50) }}{{ (strlen(strip_tags($banner->titulo)) > 50 ? "..." : "") }}</h2></strong>
                    <a href="{{ route('post.item', $banner->slug)}}" class="button-carousel">Clique aqui</a>
                </div>
              </div>
            </div>
          @endforeach
          <span class="prev"><img src="theme/images/prev2.png" alt="<"></span>
          <span class="next"><img src="theme/images/next2.png" alt=">"></span>
        </div>
      @endif
  </section>

  <section class="posts">
    <div class="container">
      <h2>Últimas postagens</h2>
      <div class="row">
        @foreach($posts as $post)
          <div class="col-md-4 post">
            <div class="card">
              <a href="{{ route('post.item', $post->slug)}}">
                <div class="post-image" style="background-image: url('http://pjasolucoes.com.br/uploads/post/{{$post->imagem}}');">
                  <span class="categoria">
                    @if($post->categoria === "financas")
                      <a href="{{ route('financa')}}" class="cat-selecionada">Finanças</a>
                    @elseif($post->categoria === "vendereceber")
                      <a href="{{ route('venderereceber')}}" class="cat-selecionada">Vender e Receber</a>
                    @else
                      <a href="{{ route('namidia')}}" class="cat-selecionada">Na Mídia</a>
                    @endif
                  </span>
                </div>
              </a>
              <div class="post-txt">
                @foreach($user->where('nome',$post->autor) as $autor)
                  <div class="avatar" style="background-image: url('https://www.pjasolucoes.com.br/uploads/users/{{$autor->imagem}}')">
                  </div>
                @endforeach
                <span class="autor-post">{{$post->autor}}</span>
                <a href="{{ route('post.item', $post->slug)}}">
                  <h3>{{$post->titulo}}</h3>
                </a>
                <a href="{{ route('post.item', $post->slug)}}">
                  <p class="post-resumo">{{ substr(strip_tags($post->texto), 0, 100) }}{{ (strlen(strip_tags($post->texto)) > 100 ? "..." : "") }}...</p>
                </a>
                <div class="cat-dat">
                  <?php
                    date_default_timezone_set('America/Sao_Paulo');
                    setlocale(LC_ALL, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
                    setlocale(LC_TIME, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
                   ?>

                  <span class="post-data">{{ Carbon\Carbon::parse($post->datapublicacao)->formatLocalized('%d de %B de %Y') }}</span>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      {{ $posts->links() }}
    </div>
  </section>

  @include('pages.partials._footer')

@endsection

@section('script')
<script src="{{ asset('theme/js/jquery-1.9.1.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('theme/js/bootstrap.min.js') }}" charset="utf-8"></script>

@include('pages.partials._scripts')
<script type="text/javascript">
$(document).ready(function(){

function mostrarBanner(indice){
  clearInterval(executar);
    // if(mouse) return;
    $('.carousel-banner .item-carousel').css({'opacity':'0','z-index':'0'});
    $('.carousel-banner .item-carousel').eq(banner).css({'opacity':'1','z-index':'1'});

    var pausar = false

      executar = setInterval(function() {
        if (pausar) return
        $('.carousel-banner .next').trigger('click');
      }, 7000);

      $('#banner').hover(function() {
        pausar = true
      }, function() {
        pausar = false
      });

}
var executar = setInterval(function(){ $('.carousel-banner .next').trigger('click'); }, 7000);
var banner = 0;
mostrarBanner(banner);

$('.carousel-banner .prev').click(function(){
    mouse = false;
    banner = (banner == 0) ? $('.carousel-banner .item-carousel').length-1 : banner-1;
mostrarBanner(banner);
});

$('.carousel-banner .next').click(function(){
    mouse = false;
    banner = ($('.carousel-banner .item-carousel').length-1 > banner) ? banner+1 : 0;
mostrarBanner(banner);
});
});
</script>
@endsection
