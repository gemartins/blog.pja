@extends('admin.main')

@section('page-title')
Editar Post
@endsection

@section('page-caminho')
Post Cliente
@endsection

@section('script-bottom')
<link href="{{ asset('template/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="modal fade" id="modal-default">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="row">
        <div class="form-group col-md-12">
          <img src="{{ asset('uploads/post/'.$post->imagem) }}" style="width: 50%">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-12">
  <div class="card-box">
    {{ Form::model($post, ['route' => ['post.update', $post->id], 'method' => 'PUT', 'files' => true]) }}

          <div class="row">
            <div class="form-group col-md-6">
              {{ Form::label('titulo', 'Título do Post') }}
              {{ Form::text('titulo', null, array('class' => 'form-control', 'autofocus','maxlength' => '150')) }}
            </div>
            <div class="form-group col-md-3">
              {{ Form::label('categoria', 'Categoria do Post') }}
                <select class="form-control" name="categoria">
                  <option value="vendereceber"> Vender e Receber </option>
                  <option value="financas"> Finanças </option>
                  <option value="midia"> Na Mídia </option>
                </select>
            </div>
            <div class="form-group col-md-3">
              {{ Form::label('postprincipal', 'Post Principal') }}
              <br>
              {{ Form::label('postprincipal', 'Sim') }}
              {{ Form::radio('postprincipal', 's' , false) }}
              {{ Form::label('postprincipal', 'Não') }}
              {{ Form::radio('postprincipal', 'n' , true) }}
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-7">
              {{ Form::label('imagem', 'Imagem do post') }}
              <input type="file" name="imagem" class="filestyle" data-placeholder="{{$post->imagem}}" data-btnClass="btn-light">
            </div>
            <div class="form-group col-md-5">
              {{ Form::label('imagem', 'Imagem Cadastrada') }}
              <button type="button" class="btn btn-info btn-lg " data-toggle="modal" data-target="#modal-default">Abrir imagem</button>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-12">
              {{ Form::label('texto', 'Texto da Postagem') }}
              {!! Form::textarea('texto', null, array('class' => 'form-control', 'autofocus')) !!}
            </div>
          </div>

          <div class="row" style="margin-top: 20px">
            <div class="form-group col-12">
              <div class="text-center">
                <button class="btn btn-success" type="submit" value="Salvar"><i class="fa fa-save m-r-5"></i> Atualizar</button>
                <a href="{{ route('post.index') }}" class="btn btn-danger"><i class="fa fa-window-close m-r-5"></i> Cancelar</a>
              </div>
            </div>
          </div>
    {{ Form::close() }}
  </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('template/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('template/js/autosize.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/plugins/ckeditor5/ckeditor.js')}}"></script>
<script src="{{ asset('template/plugins/tinymce/tinymce.min.js') }}"></script>

<script>
jQuery(function($){
  $('.js-example-basic-single').select2();
});

autosize(document.querySelectorAll('textarea'));

var editor_config = {
    path_absolute : "/",
    selector: "textarea#texto",
    height:300,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }
      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };
  tinymce.init(editor_config);
</script>
@endsection
