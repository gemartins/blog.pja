<?php

Route::get('/', 'WebSiteController@home')->name('home');

Route::get('/tela', 'WebSiteController@login')->name('login');

Route::get('/post/{slug}', ['as' => 'post.item', 'uses' => 'WebSiteController@post'])->where('slug', '[\w\d\-\_]+');



Route::get('/categoria/financa', 'WebSiteController@financa')->name('financa');

Route::get('/categoria/vender_e_receber', 'WebSiteController@venderereceber')->name('venderereceber');

Route::get('/categoria/na_midia', 'WebSiteController@namidia')->name('namidia');




Route::get('/pagenotfound', 'WebSiteController@pagenotfound')->name('pagenotfound');

Route::group(['prefix' => 'admin', 'middleware' => 'auth:web'], function () {
    Route::get('/', 'HomeController@index')->name('admin.home');

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::resource('post', 'PostController');

    Route::resource('historico', 'HistoricoController');

    Route::resource('usuario', 'UserController');
    Route::get('usuario/{usuario}/editar_senha', 'UserController@editPassword')->name('usuario.editar_senha');
    Route::post('usuario/atualizar_senha/{usuario}', 'UserController@updatePassword')->name('usuario.atualizar_senha');
});

Auth::routes();
