<?php
namespace App\Http\Controllers;
use App\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Carbon\Carbon;

use DB;
use Image;
use Storage;

class PostController extends Controller
{
    public function index(Request $request)
    {
      $posts = Post::all();

      return view('admin.post.index', compact('posts'));
    }

    public function create()
    {
      $post = Post::all();

      return view('admin.post.create', compact('post'));
    }

    public function tirarAcentos($string)
    {
      $string = preg_replace('/[áàãâä]/ui', 'a', $string);
      $string = preg_replace('/[éèêë]/ui', 'e', $string);
      $string = preg_replace('/[íìîï]/ui', 'i', $string);
      $string = preg_replace('/[óòõôö]/ui', 'o', $string);
      $string = preg_replace('/[úùûü]/ui', 'u', $string);
      $string = preg_replace('/[ç]/ui', 'c', $string);
      $string = preg_replace('/[^a-z0-9]/i', '_', $string);
      $string = preg_replace('/_+/', '_', $string);
      return $string;
    }

    public function store(Request $request)
    {
      $this->validate($request, array(
        'titulo'          => 'required',
        'texto'           => 'required',
        'categoria'       =>'required'
      ));

      $slug = Self::tirarAcentos(str_replace(" ", "-", $request->titulo));

      $post = new Post;
      $post->titulo = $request->titulo;
      $post->texto  = $request->texto;
      $post->categoria  = $request->categoria;
      $post->slug   = $slug;
      $post->postprincipal = $request->postprincipal;

      $post->datapublicacao = Carbon::now();
      $post->datapublicacao->toDateString();
      $post->autor = \Auth::user()->nome;
      $post->imagemautor = \Auth::user()->imagem;

      $texto = $post->texto;

      $subtesto = str_replace("/photos/", 'www.pjasolucoes.com.br/photo/', $texto);

      $post->texto = $subtesto;

      if ($request->hasFile('imagem')) {
        $image = $request->file('imagem');
        $filename = time() . '.' . $image->getClientOriginalName();
        $location = public_path('uploads/post/' . $filename);
        Image::make($image)->save($location);
        $post->imagem = $filename;
      }

      $post->save();

      $request->session()->flash('success', 'Post '.$post->titulo.' adicionada com sucesso.');

      return redirect()->route('post.index');
    }

    public function edit($id, Request $request)
    {
      $post = Post::findOrFail($id);
      return view('admin.post.edit', compact('post'));
    }

    public function update(Request $request, $id)
    {
      $post = Post::find($id);

      $post->fill($request->all());

      if ($request->hasFile('imagem')) {
        $image = $request->file('imagem');
        $filename = time() . '.' . $image->getClientOriginalName();
        $location = public_path('uploads/post/' . $filename);
        Image::make($image)->save($location);

        if ($post->credores) {
          $oldFilename = $post->credores;
          Storage::delete('uploads/credor/'.$oldFilename);
        }
        $post->imagem = $filename;
        }

        $slug = Self::tirarAcentos(str_replace(" ", "-", $request->titulo));
        $post->slug = $slug;

        $post->save();
        $request->session()->flash('success', 'Post foi modificado com sucesso');
        return redirect('admin/post');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return [response()->json("success"), redirect('admin/post')];
    }
}
