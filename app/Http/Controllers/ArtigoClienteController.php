<?php

namespace App\Http\Controllers;
use App\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ArtigoDevedor;
use App\Models\ColecaoDevedor;
use Illuminate\Http\Request;
use DB;
use Image;
use Storage;

class ArtigoClienteController extends Controller
{
  public function index($colecao)
  {
    $artigo = ArtigoDevedor::paginate(500)->where('colecao_id', $colecao);

    return view('admin.artigocliente.index', ['artigo' => $artigo,'colecao' => $colecao]);
  }
  public function create()
  {
    $artigo  = ArtigoDevedor::all();
    $colecao = ColecaoDevedor::all();

    return view('admin.artigocliente.create', ['artigo' => $artigo], ['colecao' => $colecao]);
  }

  public function tirarAcentos($string)
  {
    // return preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    $string = preg_replace('/[áàãâä]/ui', 'a', $string);
    $string = preg_replace('/[éèêë]/ui', 'e', $string);
    $string = preg_replace('/[íìîï]/ui', 'i', $string);
    $string = preg_replace('/[óòõôö]/ui', 'o', $string);
    $string = preg_replace('/[úùûü]/ui', 'u', $string);
    $string = preg_replace('/[ç]/ui', 'c', $string);
    // $string = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $string);
    $string = preg_replace('/[^a-z0-9]/i', '_', $string);
    $string = preg_replace('/_+/', '_', $string); // ideia do Bacco :)
    return $string;
  }

  public function store(Request $request)
  {
    $this->validate($request, array(
      'titulo'           => 'required',
      'texto'            => 'required',
    ));

    $slug = Self::tirarAcentos(str_replace(" ", "-", $request->titulo));

    $artigo = new ArtigoDevedor;
    $artigo->titulo  = $request->titulo;
    $artigo->autor = \Auth::user()->nome;
    $artigo->imagemautor = \Auth::user()->imagem;
    $artigo->descricao       = $request->descricao;
    $artigo->texto       = $request->texto;
    $artigo->colecao_id       = $request->colecao_id;
    $artigo->slug            = $slug;

    $artigo->save();

    return redirect('admin/artigocliente/'.$artigo->colecao_id)->with('flash_message', 'Artigo Devedor adicionado com sucesso!');
  }

  public function edit($id)
  {
    $artigo = ArtigoDevedor::find($id);
    $colecao = ColecaoDevedor::all();

    return view('admin.artigocliente.edit', ['artigo' => $artigo,'colecao' => $colecao]);
  }

  public function update(Request $request, $id)
  {
    $artigo = ArtigoDevedor::find($id);
    $colecao = ColecaoDevedor::all();

    $slug = Self::tirarAcentos(str_replace(" ", "-", $request->titulo));
    $artigo->fill($request->all());
    $artigo->slug = $slug;

    $artigo->save();

    $request->session()->flash('success', 'Artigo Devedor alterado com sucesso.');
    return redirect('admin/artigocliente/'.$artigo->colecao_id);
  }

  public function destroy($id)
  {
    $artigo = ArtigoDevedor::find($id);
    $artigo->delete();

    return [response()->json("success"), redirect('admin/artigocliente')];
  }
}
