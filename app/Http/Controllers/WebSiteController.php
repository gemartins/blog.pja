<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa;
use App\Models\Post;
use App\User;

use DB;

class WebSiteController extends Controller
{
    public function home()
    {
      $empresa = Empresa::find(1);
      $posts = DB::table('post')->where('deleted_at', '=', NULL)->orderBy('created_at', 'desc')->paginate(9);
      $banners = Post::where('postprincipal', '=', 's')->take(3)->get();
      $user = User::all();

      return view('pages.index', compact('empresa','posts','banners','user'));
    }

    public function post($slug){
      $empresa = Empresa::find(1);
      $post = Post::where('slug', '=', $slug)->first();
      $user = User::all();

      return view('pages.post', compact('empresa','post'));
    }

    public function financa(){
      $empresa = Empresa::find(1);
      $posts = Post::where('categoria', '=', "financas")->paginate(9);
      $post = $posts[0];
      $user = User::all();

      return view('pages.categorias', compact('empresa','posts','post','user'));
    }

    public function venderereceber(){
      $empresa = Empresa::find(1);
      $posts = Post::where('categoria', '=', "vendereceber")->paginate(9);
      $post = $posts[0];
      $user = User::all();

      return view('pages.categorias', compact('empresa','posts','post','user'));
    }

    public function namidia(){
      $empresa = Empresa::find(1);
      $posts = Post::where('categoria', '=', "midia")->paginate(9);
      $post = $posts[0];
      $user = User::all();

      return view('pages.categorias', compact('empresa','posts','post','user'));
    }




    public function pagenotfound()
    {
      $empresa = Empresa::find(1);

      $page = 'erro404';
      return view('pages.pagenotfound', compact('empresa'));
    }

    public function login()
    {
      $empresa = Empresa::find(1);

      return view('pages.login', compact('empresa'));
    }
}
