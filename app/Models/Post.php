<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

class Post extends Model
{
    protected $table = 'post';
    protected $dates = ['deleted_at'];
    protected $fillable = ['id',
    'titulo',
    'texto',
    'categoria',
    'imagem',
    'slug',
    'autor',
    'imagemautor',
    'postprincipal'
    ];

    use SoftDeletes, CascadeSoftDeletes;
}
