<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

class ArtigoDevedor extends Model
{
    protected $table = 'artigodevedor';
    protected $dates = ['deleted_at'];
    protected $fillable = ['id',
    'titulo',
    'descricao',
    'texto',
    'slug',
    'autor',
    'colecao_id',
    'imagemautor'
    ];

    use SoftDeletes, CascadeSoftDeletes;
}
