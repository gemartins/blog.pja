<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

class Empresa extends Model
{
    protected $table = 'empresa';
    protected $dates = ['deleted_at'];
    protected $fillable = ['id',
    'nomefantasia',
    'telefone_credor',
    'telefone_comercial',
    'instagram',
    'facebook',
    'youtube',
    'linkedin',
    'endereco',
    'blog',
    'email',
    'whatsapp',
    'areadocliente',
    'palavraschave',
    'descricao',
    ];

    use SoftDeletes, CascadeSoftDeletes;
}
