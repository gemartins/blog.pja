<?php
namespace App\Providers;
use App\User;

use App\Models\Aplicativo;
use App\Models\Empresa;
use App\Models\Cliente;
use App\Models\Comentario;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Schema::defaultStringLength(191);
      Carbon::setLocale(env('LOCALE', 'pt_BR'));

      \Carbon\Carbon::setLocale($this->app->getLocale());
      setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

        //Tabela da Empresa
        // Empresa::saved(function ($dados) {
        //     DB::table('logs')->insert([
        //         'user' => \Auth::user()->nome,
        //         'descricao' => $dados,
        //         'acoes' => 'Editou as informações da Empresa',
        //         'date' => $dados->updated_at
        //     ]);
        // });
        // //Tabela Aplicativo
        // Aplicativo::saved(function ($dados) {
        //     DB::table('logs')->insert([
        //         'user' => \Auth::user()->nome,
        //         'descricao' => $dados,
        //         'acoes' => 'Editou as informações do Aplicativo',
        //         'date' => $dados->updated_at
        //     ]);
        // });
        // //Tabela de Clientes
        // Cliente::saved(function ($dados) {
        //   $msg = ($dados->created_at == $dados->updated_at) ? 'Adicionou' : 'Editou';
        //   $date = ($dados->created_at == $dados->updated_at) ? $dados->created_at : $dados->updated_at;
        //       DB::table('logs')->insert([
        //           'user' => \Auth::user()->nome,
        //           'descricao' => $dados,
        //           'acoes' =>$msg.' o cliente "'.$dados->nome.'"',
        //           'date' => $date
        //       ]);
        //   });
        // Cliente::deleted(function ($dados) {
        //     DB::table('logs')->insert([
        //         'user' => \Auth::user()->nome,
        //         'descricao' => $dados,
        //         'acoes' => 'Deletou o cliente "'.$dados->nome.'"',
        //         'date' => $dados->deleted_at
        //     ]);
        // });
        // //Tabela de Comentarios
        // Comentario::saved(function ($dados) {
        //   $msg = ($dados->created_at == $dados->updated_at) ? 'Adicionou' : 'Editou';
        //   $date = ($dados->created_at == $dados->updated_at) ? $dados->created_at : $dados->updated_at;
        //       DB::table('logs')->insert([
        //           'user' => \Auth::user()->nome,
        //           'descricao' => $dados,
        //           'acoes' =>$msg.' o comentário de "'.$dados->nome.'"',
        //           'date' => $date
        //       ]);
        //   });
        // Comentario::deleted(function ($dados) {
        //     DB::table('logs')->insert([
        //         'user' => \Auth::user()->nome,
        //         'descricao' => $dados,
        //         'acoes' => 'Deletou o comentário de "'.$dados->nome.'"',
        //         'date' => $dados->deleted_at
        //     ]);
        // });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
